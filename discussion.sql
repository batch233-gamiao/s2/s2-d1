-- List down the databases inside the DBMS

SHOW DATABASES;

-- Create a database

CREATE DATABASE music_db;

-- Remove database

DROP DATABASE music_db;

-- Select database

USE music_db;

-- Create tables
-- Table columns have the following format: [column name] [data type] [other options]
	-- NOT NULL means it is required
	-- AUTO INCREMENT id to have different value (1 -> 2 -> 3 -> ...)
	-- VARCHAR(characterLimit) to avoid data bloating

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT, 
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

/*
	mini-activity
		1. create a table for artists
		2. artists should have an id
		3. artists is required to have a name with 50 character limits
		4. assign the primary key to its id
		5. send a screenshot of your myphpadmin in Hangouts
*/

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT, 
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT, 
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


/*
	mini-activity
	1. Create a table for songs
	2. Put an auto increment id
	3. declare a song name with 50 char limits, this should be required
	4. declare a length with the data type time and it should be required
	5. declare a genre with 50 char limit, it should be required
	6. declare an integer as album id that should be required
	7. create a primary key referring to the id of the songs
	8. create a foreign key and name it fk_songs_album_id
		8.a this should be referred to the album id
		8.b it should have a cascaded update and restricted delete
	9. run both create table album and create table songs 
	10. send your output in Hangouts
*/

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT, 
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY(id)
);

